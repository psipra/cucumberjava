package artOfTesting;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
    features= "src/test/resources/artOfTesting/test"
    ,glue= {"artOfTesting.test"})
public class googleCalcTest {
}
